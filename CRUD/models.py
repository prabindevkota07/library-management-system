from django.db import models

# Create your models here.

class Crud(models.Model):
    Name = models.CharField(max_length=122)
    Email = models.EmailField(null=False)
    Contact = models.IntegerField()

    def __str__(self):
        return self.Name
