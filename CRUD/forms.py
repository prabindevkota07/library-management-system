from django import forms
from django.core.exceptions import ValidationError
from .models import Crud


class Student(forms.ModelForm):
    class Meta:
        model = Crud
        fields = '__all__'


    def __init__(self, *args, **kwargs):
        super(Student, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            self.fields['Name'].widget.attrs.update({'placeholder': 'Enter Your Name'})
            self.fields['Email'].widget.attrs.update({'placeholder': 'name@mail.com'})


