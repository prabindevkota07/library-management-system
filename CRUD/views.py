from .forms import Student
from .models import Crud
from django.contrib import messages
from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
from django.views import generic
from django.views.generic import View, FormView, RedirectView, DeleteView



# Create your views here.

def index(request):
    if request.method == "POST":
        form = Student(request.POST or none)
        if form.is_valid():
            form.save()
            eform = Student()
            list = Crud.objects.all()
            context = {
                'list': list,
                'form': eform
            }
            messages.success(request, 'Success')
            return render(request, 'index.html',context)
        else:
            list = Crud.objects.all()
            context = {
                'list': list,
                'form': form
            }
            return render(request, 'index.html',context)
    else:
        list = Crud.objects.all()
        form = Student()
        context = {
            'form': form,
            'list': list
        }
        return render(request, 'index.html', context)


def update(request, id):
    if request.method == 'GET':
        update_data = get_object_or_404(Crud, pk = id)
        uform = Student(instance=update_data)
        details =Crud.objects.all()
        context = {
            'details': details,
            'form': uform
        }
        return render(request, 'update.html', context)
    if request.method == "POST":
        update_data = get_object_or_404(Crud, pk=id)
        uform = Student(request.POST, instance= update_data)
        if uform.is_valid():
            uform.save()
            eform = Student()
            list = Crud.objects.all()
            context = {
                'list': list,
                'form': eform
            }
            return render(request, 'index.html',context)
        else:
            context = {

                'form': uform
            }
            return render(request, 'update.html', context)

def delete(request, id):
    delete_data = Crud.objects.get(pk=id)
    delete_data.delete()
    return redirect('index')